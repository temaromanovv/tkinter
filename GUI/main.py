from tkinter import *


root = Tk()
root.title('My application')
root.geometry('640x480')
root.resizable(width=False, height=False)

cnt = 0

def click():
    global cnt
    cnt += 1
    print(f'Количество кликов {cnt}')

btn = Button(text='Ваш вес',
             command=click)
btn.pack()

if __name__ == '__main__':
    root.mainloop()